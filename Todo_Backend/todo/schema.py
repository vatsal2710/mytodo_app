import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from .models import ToDo


class ToDoType(DjangoObjectType):
    class Meta:
        model = ToDo


class Query(ObjectType):

    task = graphene.Field(ToDoType, id=graphene.Int())
    tasks = graphene.List(ToDoType)

    def resolve_task(self, info, **kwargs):

        id = kwargs.get("id")

        if id is not None:
            return ToDo.objects.get(pk=id)

        return None

    def resolve_tasks(self, info, **kwargs):
        return ToDo.objects.all()


class ToDOInput(graphene.InputObjectType):
    id = graphene.ID()
    Task = graphene.String()
    Description = graphene.String()
    Date = graphene.Date()
    Time = graphene.Time()


class CreateToDo(graphene.Mutation):
    class Arguments:
        input = ToDOInput(required=True)

    ok = graphene.Boolean()
    todo = graphene.Field(ToDoType)

    @staticmethod
    def mutate(root, info, input=None):
        ok = True

        print("------")
        print(input)
        todo_instance = ToDo(
            Task=input.Task,
            Description=input.Description,
            Date=input.Date,
            Time=input.Time,
        )

        todo_instance.save()
        return CreateToDo(ok=ok, todo=todo_instance)


class UpdateToDo(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = ToDOInput(required=True)

    ok = graphene.Boolean()
    todo = graphene.Field(ToDoType)

    @staticmethod
    def mutate(root, info, id, input=None):
        ok = False
        todo_instance = ToDo.objects.get(pk=id)
        if todo_instance:
            ok = True
            todo_instance.Task = input.Task
            todo_instance.Description = input.Description
            todo_instance.Date = input.Date
            todo_instance.Time = input.Time
            todo_instance.save()
            return UpdateToDo(ok=ok, todo=todo_instance)
        return UpdateToDo(ok=ok, todo=None)


class DeleteToDo(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)

    ok = graphene.Boolean()

    @staticmethod
    def mutate(self, info, id):
        ok = True
        link = ToDo.objects.get(id=id)
        print("DEBUG: ${link.id}:${link.Description}")
        link.delete()
        return DeleteToDo(ok=ok)


class Mutation(graphene.ObjectType):
    create_todo = CreateToDo.Field()
    update_todo = UpdateToDo.Field()
    delete_todo = DeleteToDo.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
