from django.db import models

# Create your models here.


class ToDo(models.Model):

    Task = models.CharField(max_length=100)
    Description = models.TextField(max_length=255)
    Date = models.DateField()
    Time = models.TimeField(auto_now=False)

    def __str__(self):
        return self.Task