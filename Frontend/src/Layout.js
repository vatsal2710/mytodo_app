import React, { Component } from 'react'
import { Link, Route, Switch } from 'react-router-dom'
import './Layout.css'
import Home from './Home'
import Add from './Add'
class Layout extends Component {
    render() {
        return (
            <div>
                <header className="header">
                    <nav>
                        <ul>
                           <li><Link to="/" className="link">Home</Link></li>
                            <li><Link to="/add" className="link">Add-ToDo</Link></li>
                            <li><Link to="/update" className="link">Update</Link></li>
                        </ul>
                    </nav>
                </header>

                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/add" component={Add} />

                </Switch>
            </div>
        )
    }
}

export default Layout
