import React, { Component } from 'react'
import { gql } from '@apollo/client';
import { client } from './index'
class Add extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             Task : '',
             Description : '',
             Date : '',
             Time : ''
        }
    }

    handleSubmit(e){
        e.preventDefault();
        console.log(this.state.Task)
        console.log(this.state.Date)
        console.log(this.state.Time)


    // --------------------- DELETE TODO -------------------------

    // const variables = {
    //     id : 4
    // } 

    // client
    //   .mutate({
    //     mutation: gql`
    //       mutation deleteTodo($id: Int!) {
    //         deleteTodo(id: $id) {
    //           ok
    //         }
    //       }
    //     `,
    //     variables: variables,
    //   })


    // --------------------- UPDATE TODO -------------------------

    // const variables = {
    //     id : 4
    // } 

    // client
    // .mutate({
    //   mutation : gql`
    //     mutation updateTodo($Task: String!,$Description : String!,$Date : Date!,$Time: Time!) {
    //       updateTodo(id:1, input : {Task: $Task,Description : $Description,Date : $Date, Time : $Time }) {
          
    //          ok
              
    //       }
    //     }
    //   `,
    //   variables: variables,
    // })
    //   .then((result)=>console.log(result))
    //       .catch((e)=>console.log(e))

    //   }

        const variables = {
        Task : this.state.Task,
        Description : this.state.Description,
        Date : this.state.Date,
        Time : this.state.Time, 
        };

        console.log(`----- `)
        console.log(variables)


    client
      .mutate({
        mutation : gql`
          mutation createTodo($Task: String!,$Description : String!,$Date : Date!,$Time: Time!) {
            createTodo( input : {Task: $Task,Description : $Description,Date : $Date, Time : $Time }) {
            
               ok
                
            }
          }
        `,
        variables: variables,
      })
        .then((result)=>console.log(result))
            .catch((e)=>console.log(e))

        }

    
    
    render() {
        return (
            <div className="container">
                <h1>Add ToDo </h1>


                <form onSubmit={(e) => this.handleSubmit(e)} className="Form">
                    <label>
                        Task :
                        <input type="text" placeholder="task name" name="Task" value={this.state.Task} onChange={(e)=>this.setState({Task:e.target.value})}></input>
                    </label>
                    <label>
                        Description :
                        <input type="text" placeholder="task description" name="Task" value={this.state.Description} onChange={(e)=>this.setState({Description:e.target.value})}></input>
                    </label>

                    <label>
                        Date :
                        <input type="date" name="Date" value={this.state.Date} onChange={(e)=>this.setState({Date:e.target.value})}></input>
                    </label>

                    <label>
                        Time : 
                        <input type="time" name="Time" value={this.state.Time} onChange={(e)=>this.setState({Time:e.target.value})} ></input>
                    </label>
       
                    <input type="submit" value="Submit" />
                </form>

            </div>
        )
    }
}

export default Add
