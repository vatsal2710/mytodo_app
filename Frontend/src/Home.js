import React, { Component } from 'react'
import { gql} from '@apollo/client';
import { client } from './index'
import './Home.css'
import Demo from './Demo'


class Home extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             tasks : []
        }
    }
    

    componentDidMount(){
      
    client
        .query({
            query:gql`
            query tasks {
                tasks {
                  id
                  Task
                  Description
                  Date
                  Time
                }
              }
            `,
            variables: {},
            })
            .then((result)=>this.setState({tasks:result.data.tasks},()=>console.log(this.state.tasks)))
            .catch((e)=>console.log(e))


        
    }
    render() {
        const R = this.state.tasks.map(item => {
            return <div className="task-box">
                <h3>{item.Task}</h3>
                <p>{item.Description}</p>
                <p>{item.Date}</p>
                <p>{item.Time}</p>
            </div>
        })
        return (
            <div>
                <h1>Home</h1>
                {R}

                <h4> Redux Demo </h4>
                <Demo />
            </div>
        )
    }
}

export default Home
